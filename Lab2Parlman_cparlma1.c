#include <getopt.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>

int main(int argc, char** argv){

	pid_t pid = getpid();//= getpid();
	pid_t ppid =getppid();

	//flags 
	int uflag = 0;
	int Nflag = 0;
	int Mflag = 0;
	int pflag = 0;
	int sflag = 0;

	//defaults
	char mflag;

	//max 
	const short MAX_LEVELS = 4;
	const short MAX_CHILD  = 3;

	//get arguments... flag the flags...
	int flag;
	while((flag = getopt(argc,argv, "uN:M:ps:"))!= -1)
		switch(flag){
			case('u'):
				fprintf(stdout, "usage string\n");					
				uflag = 1;
				break;
			case('N'):
				Nflag = atoi(optarg);
				
				//printf("Nflag = %d\n", Nflag);
				break;
			case('M'):
				Mflag = atoi(optarg);
			//	mflag = *optarg;
				//printf("Mflag = %c\n", mflag);
				break;
			case('p'):
				pflag = 1;
				break;
			case('s'):
				sflag = atoi(optarg);
				break;
			case('?'):
				fprintf(stderr,"usage string\n");	
				exit(0);
			case(';'):
				fprintf(stderr, "case(':')\n");					
				fprintf(stderr,"usage string\n");	
				exit(0);
				break;
		}
	//unspecified m flag default is 1
	if(!Mflag)
		Mflag = 1;
	if(!pflag && !sflag)
		sflag = 1;
	if(pflag && sflag){
		fprintf(stderr,"usage string\n");	
		exit(0);
	}


	if(uflag){
		fprintf(stderr, "usage string\n");
	}
	else{
		if(Nflag == 1 || Nflag == 0){
			fprintf(stdout,"ALIVE  : Level %d process with pid=%d, ",Nflag-1,pid);
			fprintf(stdout,"child of ppid=%d.\n",ppid);
		}
		else if(Nflag > 4){
			fprintf(stderr,"Nflag maximum of 4\n");
			fprintf(stderr, "usage string\n");
		}
		else{
			if(Mflag >3){
				fprintf(stderr,"Mflag maximum of 3\n");
				fprintf(stderr, "usage string\n");
			}
			else{


				fprintf(stdout,"ALIVE  : Level %d process with pid=%d, ",Nflag-1,pid);
				fprintf(stdout,"child of ppid=%d.\n",ppid);

				char* n = malloc(50);
				char* m = malloc(50);
				char* s = malloc(50);
				sprintf(n, "%d", Nflag-1 );
				sprintf(m, "%d", Mflag );
				sprintf(s, "%d", sflag );

				if(0){
					printf("pflag=%d, Nflag=%d, Mflag=%d\n",pflag, Nflag, Mflag);
				}

				int re = 0;	
				int next_level = 0;
	//			printf("%d, %d\n",Nflag, sflag);
				
				//if(Nflag){
					int i = 0;
					for(; i < Mflag;i++){
						pid = fork();
					}
						if(pid < 0) {/* error */
							fprintf(stderr, "Fork failed.");
							return 1;
						}
						if(pid == 0){ /* child process*/ //execlp("/bin/ls", "ls",NULL);

							pid = getpid();

							if(pflag)
								re = execlp("./lab2","./lab2","-N",n,"-M",m,"-p", NULL);
							else if(sflag)
								re = execlp("./lab2","./lab2","-N",n,"-M",m,"-s",s, NULL);
							else
								re = execlp("./lab2","./lab2","-N",n,"-M",m,NULL);//"-M",&m,NULL);
							if(re == -1) printf("Error\nnext_l=%d, &n=%c, &m=%c \n", next_level,&n,&m);
						}
						else{/* parent process */ /* parent will wait for the child to complete */
							wait(NULL);
						}
				//}
			//	fprintf(stdout,"EXITING: Level %d process with pid=%d, ",Nflag,pid);
			//	fprintf(stdout,"child of ppid=%d.\n",ppid);
			}
		}
	}
				if(Nflag <= 1 && sflag)
					sleep(sflag);
				else if(Nflag <= 1 && pflag ){
					pause();
				}
			fprintf(stdout,"EXITING: Level %d process with pid=%d, ",Nflag-1,pid);
			fprintf(stdout,"child of ppid=%d.\n",ppid);
//	return 0;
	}
